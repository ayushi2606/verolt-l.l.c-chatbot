function showSidebar() {
    const sidebar = document.querySelector('.nav_links-sidebar');
    sidebar.style.display = "flex";
}

function hideSidebar() {
    const sidebar = document.querySelector('.nav_links-sidebar');
    sidebar.style.display = "none";
}

class Chatbox {
    constructor() {
        this.args = {
            openButton: document.querySelector('.chatbox__button'),
            chatBox: document.querySelector('.chatbox__support'),
            sendButton: document.querySelector('.send__button')
        };

        this.state = false;
        this.messages = [];
        this.responses = {}; // Object to store responses from intents.json
    }

    // Load responses from intents.json
    async loadResponses() {
        try {
            const response = await fetch('intents1.json'); // Assuming intents.json is in the same directory
            this.responses = await response.json();
        } catch (error) {
            console.error('Error loading responses:', error);
        }
    }

    async display() {
        await this.loadResponses(); // Load responses before displaying

        const { openButton, chatBox, sendButton } = this.args;

        openButton.addEventListener('click', () => this.toggleState(chatBox));

        sendButton.addEventListener('click', () => this.onSendButton(chatBox));

        const node = chatBox.querySelector('input');
        node.addEventListener('keyup', ({ key }) => {
            if (key === 'Enter') {
                this.onSendButton(chatBox);
            }
        });
    }

    toggleState(chatBox) {
        this.state = !this.state;

        if (this.state) {
            chatBox.classList.add('chatbox--active');
        } else {
            chatBox.classList.remove('chatbox--active');
        }
    }

    onSendButton(chatBox) {
        const textField = chatBox.querySelector('input');
        const text1 = textField.value;

        if (text1 === '') {
            return;
        }

        const msg1 = { name: 'User', message: text1 };
        this.messages.push(msg1);

        const intent = this.determineIntent(text1);
        const response = this.getRandomResponse(intent);
        const msg2 = { name: 'Sam', message: response };
        this.messages.push(msg2);

        this.updateChatText(chatBox);
        textField.value = '';
    }

    determineIntent(text) {
        const lowerText = text.toLowerCase();
        for (const intent of this.responses.intents) {
            for (const pattern of intent.patterns) {
                if (lowerText.includes(pattern.toLowerCase())) {
                    return intent;
                }
            }
        }
        return null;
    }

    getRandomResponse(intent) {
        if (intent && intent.responses.length > 0) {
            const randomIndex = Math.floor(Math.random() * intent.responses.length);
            return intent.responses[randomIndex];
        }
        return 'I am not sure how to respond to that.';
    }

    updateChatText(chatBox) {
        let html = '';
        this.messages.slice().reverse().forEach(function (item) {
            if (item.name === 'Sam') {
                html += '<div class="messages__item messages__item--visitor">' + item.message + '</div>';
            } else {
                html += '<div class="messages__item messages__item--operator">' + item.message + '</div>';
            }
        });
        const chatMessages = chatBox.querySelector('.chatbox__messages');
        chatMessages.innerHTML = html;
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const chatbox = new Chatbox();
    chatbox.display();
});

